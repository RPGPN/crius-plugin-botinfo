module gitlab.com/RPGPN/crius-plugin-botinfo

go 1.15

require (
	gitlab.com/RPGPN/crius-utils v1.5.1
	gitlab.com/RPGPN/criuscommander/v2 v2.2.6
)
